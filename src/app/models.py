from .app import db


class OperationSum(db.Model):
    """Simple database model to track event attendees."""

    __tablename__ = 'operationsum'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    sumando01 = db.Column(db.Integer)
    sumando02 = db.Column(db.Integer)
    resultado = db.Column(db.Integer)

    def __init__(self, sumando01=None, sumando02=None):
        self.sumando01 = sumando01
        self.sumando02 = sumando02


    def sum_elements(self):
        self.resultado = self.sumando01 + self.sumando02
        return self.resultado