import os

from flask import Flask, render_template, request
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask import Response
from flask import json


database_uri = 'postgresql+psycopg2://{dbuser}:{dbpass}@{dbhost}/{dbname}'.format(
    dbuser=os.environ['POSTGRES_USER'],
    dbpass=os.environ['POSTGRES_PASSWORD'],
    dbhost=os.environ['POSTGRES_HOST'],
    dbname=os.environ['POSTGRES_DB']
)



app = Flask(__name__)
app.config.update(
    SQLALCHEMY_DATABASE_URI=database_uri,
    SQLALCHEMY_TRACK_MODIFICATIONS=False,
)

# initialize the database connection
db = SQLAlchemy(app)


from .models import OperationSum

# initialize database migration management
migrate = Migrate(app, db)


@app.route('/sumar/<sumando01>/<sumando02>', methods=['GET'])
def register_guest(sumando01=0, sumando02=0):

    # resultado = int(sumando01) + int(sumando02)

    operation_sum = OperationSum(int(sumando01), int(sumando02))

    resultado = operation_sum.sum_elements()
    db.session.add(operation_sum)
    db.session.commit()

    print(resultado)
    data = {"resultado": resultado}
    response = app.response_class(
        response=json.dumps(data),
        status=200,
        mimetype='application/json'
    )
    return response

    print (resultado)
    data = {"resultado": resultado}
    response = app.response_class(
        response=json.dumps(data),
        status=200,
        mimetype='application/json'
    )
    return response


if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)
